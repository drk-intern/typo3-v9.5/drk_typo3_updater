<?php

namespace DrkService\DrkTypo3Updater\Command;

use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(
    name: 'cms:update:source',
    description: 'Update the source code',
)]
class CmsUpdateSourceCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Updates the source code')
        ;
    }

    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    )
    {
        $success = $this->execCommand('@php vendor/bin/composer update', $output);

        exit($success ? 0 : 1);
    }
}
