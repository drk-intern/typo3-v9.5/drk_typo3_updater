<?php

namespace DrkService\DrkTypo3Updater\Command;

use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractCommand extends Command
{

    protected function isCommandOk(string $cmd): bool
    {
        $output = [];
        $exitCode = 0;
        exec($cmd, $output, $exitCode);

        return $exitCode === 0;
    }

    protected function execInternalCommand($command, InputInterface $input, OutputInterface $output)
    {
        $neededArguments = $this->getApplication()->find($command)->getDefinition()->getArguments();
        $argumentsToPass = [];

        foreach ($neededArguments as $neededArgument) {
            if ($neededArgument instanceof InputArgument) {
                $argumentsToPass[$neededArgument->getName()] = $input->getArgument($neededArgument->getName());
            }
        }
        $subInput = new ArrayInput($argumentsToPass);

        $output->writeln(str_repeat('=', 120));
        $output->writeln('⌚︎ Launching subcommand: ' . $command);
        $output->writeln(str_repeat('=', 120));

        $return = $this->getApplication()->find($command)->run($subInput, $output);
        if ($return !== 0) {
            throw new \ErrorException('Command ended with error: ' . $command);
        }
    }

    protected function prepareCommand(string $cmd)
    {
        return str_replace('@php', PHP_BINARY, $cmd);
    }

    protected function execCommand(string $cmd, OutputInterface $outputWriter): bool
    {
        $cmd = $this->prepareCommand($cmd);
        $outputWriter->write('Executing: ' . $cmd, OutputInterface::VERBOSITY_VERBOSE);
        $output = [];
        $exitCode = 0;
        exec(
            $cmd,
            $output,
            $exitCode
        );
        $outputWriter->write($output, OutputInterface::VERBOSITY_VERBOSE);
        return $exitCode === 0;
    }

    protected function getCommandOutput(string $cmd, OutputInterface $outputWriter): array
    {
        $cmd = $this->prepareCommand($cmd);
        $outputWriter->write('Executing: ' . $cmd, OutputInterface::VERBOSITY_VERBOSE);
        $output = [];
        $exitCode = 0;
        exec(
            $cmd,
            $output,
            $exitCode
        );
        $outputWriter->write($output, OutputInterface::VERBOSITY_VERBOSE);
        return $output;
    }

    protected function confirm(
        $text,
        InputInterface $input,
        OutputInterface $output
    ) {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion(
            $text,
            false
        );
        return $helper->ask($input, $output, $question);
    }

    protected function requestText(
        $text,
        InputInterface $input,
        OutputInterface $output
    ) {
        $helper = $this->getHelper('question');
        $question = new Question(
            $text,
            null
        );
        return $helper->ask($input, $output, $question);
    }
}
