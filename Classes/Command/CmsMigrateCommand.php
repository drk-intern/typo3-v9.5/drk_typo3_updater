<?php

namespace DrkService\DrkTypo3Updater\Command;

use Composer\Console\Input\InputArgument;
use Composer\Console\Input\InputOption;
use Symfony\Component\Console\Attribute\AsCommand;
use TYPO3\CMS\Core\Utility\GeneralUtility;

#[AsCommand(
    name: 'cms:migrate',
    description: 'Run all the needed migrations, see help for steps',
)]
class CmsMigrateCommand extends AbstractCommand
{
    protected array $commands = [
        '@php vendor/bin/composer require sbublies/gridtocontainer "^12.0"',
        '@php vendor/bin/drkcms cms:migrate:prepare',
        '@php vendor/bin/typo3 database:updateschema "*.add,*.change"',
        '@php vendor/bin/typo3 drktemplate:clean:duplicate-relations',
        '@php vendor/bin/typo3 database:updateschema "*.add,*.change"',
        '@php vendor/bin/typo3 backend:lockforeditors',
        '@php vendor/bin/typo3 install:fixfolderstructure',
        '@php vendor/bin/typo3 install:extensionsetupifpossible --fail-on-error',
        '@php vendor/bin/typo3 cache:flush',
        '@php vendor/bin/typo3 referenceindex:update',
        // remove with v12
        '@php vendor/bin/typo3 upgrade:run --no-interaction',
        '@php vendor/bin/composer remove sbublies/gridtocontainer',
        '@php vendor/bin/typo3 language:update',
        '@php vendor/bin/typo3 backend:unlockforeditors'
    ];

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setHelp(
                implode(
                    PHP_EOL,
                    [
                        'Run all the needed migrations, we have the following steps',
                        ...$this->commands
                    ]
                )
            )
            ->addOption(
                'removeSteps',
                null,
                InputOption::VALUE_OPTIONAL,
                'comma separated list of commands to remove from the queue',
                null
            )
        ;
    }

    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ) {
        $success = false;
        $stepsToRemove = [];

        if ($input->getOption('removeSteps') !== null) {
            $stepsToRemove = GeneralUtility::trimExplode(',', $input->getOption('removeSteps'));
        }

        foreach ($this->commands as $command) {
            if (in_array($command, $stepsToRemove)) {
                $output->writeln('<info>[!!!] skip command ' . $command . '</info>');
                continue;
            }
            $success = $this->execCommand($command, $output);
            if (!$success) {
                $output->writeln('  <error>[-] failed to execute command ' . $command . '</error>');
                $success = false;
                break;
            }
            $output->writeln('  <info>[✓] executed command ' . $command . '</info>');
        }

        exit($success ? 0 : 1);
    }
}
