<?php

namespace DrkService\DrkTypo3Updater\Command;

use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(
    name: 'cms:migrate:prepare',
    description: 'Run all the needed migrations',
)]
class CmsMigratePrepareCommand extends AbstractCommand
{
    protected array $commands = [
        [
            'description' => 'Fixes nulled values in column related_links for table tx_news_domain_model_news',
            'command' => 'echo \'UPDATE tx_news_domain_model_news SET related_links=0 WHERE related_links IS NULL\' | @php vendor/bin/typo3 database:import',
        ], [
            'description' => 'Update placeholder ###EMAILADRESSE_HIER_EINGEBEN### with bitte_aendern@domain.tld',
            'command' => 'echo \'UPDATE tt_content SET bodytext = REPLACE(bodytext, "###EMAILADRESSE_HIER_EINGEBEN###", "bitte_aendern@domain.tld") WHERE CType="mailform"\' | @php vendor/bin/typo3 database:import',
        ], [
            'description' => 'Ensure user _cli_ is enabled',
            'command' => 'echo \'UPDATE be_users SET deleted=0, disable=0 WHERE username="_cli_"\' | @php vendor/bin/typo3 database:import',
        ], [
            'description' => 'Ensure sys_file_reference has no negative PIDs',
            'command' => 'echo \'UPDATE sys_file_reference SET pid=0 WHERE pid < 0\' | @php vendor/bin/typo3 database:import',
        ], [
            'description' => 'Ensure sys_category has no negative PIDs',
            'command' => 'echo \'UPDATE sys_category SET pid=0 WHERE pid < 0\' | @php vendor/bin/typo3 database:import',
        ], [
            'description' => 'Ensure tt_content has no negative PIDs - attention if workspaces is installed',
            'command' => 'echo \'UPDATE tt_content SET pid=0 WHERE pid < 0\' | @php vendor/bin/typo3 database:import',
        ], [
            'description' => 'Ensure pages has no negative PIDs - attention if workspaces is installed',
            'command' => 'echo \'UPDATE pages SET pid=0 WHERE pid < 0\' | @php vendor/bin/typo3 database:import',
        ],[
            'description' => 'Ensure sys_file_reference has null PIDs',
            'command' => 'echo \'UPDATE sys_file_reference SET pid=0 WHERE pid IS NULL\' | @php vendor/bin/typo3 database:import',
        ]
    ];

    protected function configure()
    {
        $this
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Updates data in the database')
        ;
    }

    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ): void
    {
        $success = false;

        foreach ($this->commands as $command) {
            $output->writeln('[-] ' . $command['description']);
            $success = $this->execCommand($command['command'], $output);
            if (!$success) {
                $output->writeln('  <error>[-] failed to execute command ' . $command['command'] . '</error>');
                $success = false;
                break;
            }
            $output->writeln('  <info>[✓] executed command ' . $command['command'] . '</info>');
        }

        exit($success ? 0 : 1);
    }
}
