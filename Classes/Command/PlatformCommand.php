<?php

namespace DrkService\DrkTypo3Updater\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'platform:check',
    description: 'Checks the platform requirements',
)]
class PlatformCommand extends AbstractCommand
{
    protected static $defaultName = 'platform:check';

    protected $expectedPhpExtensions = [
        'libxml',
        'openssl',
        'zlib',
        'bcmath',
        'curl',
        'dom',
        'hash',
        'filter',
        'fileinfo',
        'gd',
        'SPL',
        'iconv',
        'intl',
        'json',
        'mbstring',
        'session',
        'PDO',
        'pdo_mysql',
        'posix',
        'readline',
        'SimpleXML',
        'soap',
        'sockets',
        'sodium',
        'exif',
        'xml',
        'xmlreader',
        'xmlrpc',
        'xmlwriter',
        'xsl',
        'zip',


    ];

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Checks the platform requirements')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Checks the php platform requirements');
    }

    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ) {
        $exitCode = 0;

        $exitCode = $this->checkPhpVersion($output, $exitCode);
        $exitCode = $this->checkPhpExtensions($output, $exitCode);
        $exitCode = $this->checkComposerPlatformReqs($output, $exitCode);

        exit($exitCode);
    }

    protected function checkPhpVersion(
        \Symfony\Component\Console\Output\OutputInterface $output,
        $exitCode
    ) {
        $output->writeln('PHP Version');

        if (PHP_VERSION_ID < 80100 || PHP_VERSION_ID > 80499) {
            $output->writeln('  <error>[x] Your PHP Version does not match the requirement</error>');
            return 1;
        }
        $output->writeln('  <info>[✓] Your PHP Version ' . PHP_VERSION . ' matches the requirement</info>');
        return $exitCode;
    }

    protected function checkPhpExtensions(
        \Symfony\Component\Console\Output\OutputInterface $output,
        $exitCode
    ): int {
        $output->writeln('PHP Extensions');

        $loadedExtensions = get_loaded_extensions();

        foreach ($this->expectedPhpExtensions as $extension) {
            if (!in_array($extension, $loadedExtensions, true)) {
                $output->writeln('  <error>[-] Did not found PHP Extension: ' . $extension . '</error>');
                $exitCode = 1;
            }
            $output->writeln('  <info>[✓] Found PHP Extension: ' . $extension . '</info>');
        }
        return $exitCode;
    }

    protected function checkComposerPlatformReqs(
        \Symfony\Component\Console\Output\OutputInterface $output,
        $exitCode
    )
    {
        $output->writeln('composer check-platform-reqs');
        if (!$this->execCommand('@php vendor/bin/composer check-platform-reqs', $output)) {
            $output->writeln('  <error>[-] platform-reqs do not match for composer</error>');
            return 1;
        }
        $output->writeln('  <info>[✓] composer check-platform-reqs</info>');
        return $exitCode;
    }
}
